const bootstrapSassAbstractsImports = require('vue-cli-plugin-bootstrap-vue/sassAbstractsImports.js')
const fs = require('fs');

module.exports = {
    devServer: {
        disableHostCheck: true,
        https: {
            key: fs.readFileSync('./.cert/privkey.pem'),
            cert: fs.readFileSync('./.cert/fullchain.pem'),
        },
        port: '443',
        //host: 'hpcsera.gwdg.de'
    },
	css: {
		loaderOptions: {
			sass: {
				additionalData: bootstrapSassAbstractsImports.join('\n')
			},
			scss: {
				additionalData: [...bootstrapSassAbstractsImports, ''].join(';\n')
			}
		}
	}
};