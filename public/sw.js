//service workers are 'event' driven
//install, activate, msg => events
//functional events => fetch, push, sync

self.addEventListener('push', function (e) {
  const payload = e.data.json();
  console.log("Push Recieved...");
  console.log(e.data);
  console.log('json of payload is', payload);
  console.log('json of payload title is', payload['title']);

  /*const options = {
    //body: "Notified by Hossein HPCSerA-API",
    body: payload["title"]}
  }*/

  self.registration.showNotification(title=payload["title"], options={icon: "https://www.gwdg.de/GWDG-Theme-1.0-SNAPSHOT/images/gwdg_logo.svg"});
  /*let permission = this.confirm("Are you sure, you would like to access XXXXX endpoint?")
  let answer = this.prompt("Write yes / no in order to accept / deny permission for accessing XXXXX endpoint?")*/


  sendMessage("posting this msg to client (browser) from sw. received push notif.")
});

const sendMessage = async (msg) => {
  let allClients = [];
  allClients = await clients.matchAll({ includeUncontrolled: true });
  return Promise.all(
    allClients.map((client) => {
      // console.log('postMessage', msg, 'to', client.id);
      return client.postMessage(msg);
    })
  );
};
//https://gist.github.com/prof3ssorSt3v3/fcf37b7c9738a0d639831720f664bd1c


      //msg to main thread (can access DOM / window.prompt) that notif received
      //client.postMessage("posting this msg to client (browser) from sw. received push notif.");