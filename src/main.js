// import '@babel/polyfill'
// import 'mutationobserver-shim'
// import './plugins/bootstrap-vue'
import { createApp } from 'vue';
// import * as Vue from 'vue';
import App from './App.vue';
import router from './routers';
// import './assets/_myCustom.scss';

// import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Import Bootstrap and BootstrapVue CSS files (order is important)
// import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap-vue/dist/bootstrap-vue.css'

// import { DropdownPlugin, TablePlugin } from 'bootstrap-vue'

createApp(App).
// Vue.createApp(App).
    use(router).
    // use(TablePlugin).
    // use(DropdownPlugin).
    // use(BootstrapVue).
    // use(IconsPlugin).
    mount('#app');
