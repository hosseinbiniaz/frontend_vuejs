import subprocess
import os

# cmd1 = "echo '\n\n'; sleep 5; pwd"
# cmd2 = "echo '\n\n'; ls; echo '\n\n'"

# p1 = subprocess.Popen(cmd1, shell=True)
# p1.wait()
# p2 = subprocess.Popen(cmd2, shell=True)

path = os.getcwd();
#print(path)
#print('node {path}/../push.js'.format(path=path))


cmd1 = 'node {path}/../push.js'.format(path=path)
#start a subprocess to run the 'node push.js' - node must be installed
p1 = subprocess.Popen(cmd1, shell=True)
p1.wait()

cmd2 = "echo '\n\n'; echo 'hello world'; echo '\n\n'"
p2 = subprocess.Popen(cmd2, shell=True)

#the issue is, that push.js does not return anything
#i can still trigger the push notification here though nevertheless
#and then await a response from the client somehow
#also note node and webpush package need to be installed


