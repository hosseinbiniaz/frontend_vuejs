import js2py
from js2py import require

with open('./push-for-py.js', 'r') as file:
    data = file.read()
    #print('\n\nprinting first few lines of js push script\n\n\n' + data[:100])
    #print(type(data))
    context = js2py.EvalJs(enable_require=True)
    context.eval(data)
