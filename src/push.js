//serverside 

//delete 1st two args for node binary and push.js 
const argument = process.argv.slice(2);

// takes in as arg, the fcm_data and parse into dictionary

endpoint = argument[0]
expirationTime = argument[1]
p256dh_key = argument[2]
auth_key = argument[3]

if (expirationTime == "null") {
    expirationTime = null
}

var fcm_dict = {
    "endpoint":endpoint,
    "expirationTime":expirationTime,
    "keys":{
        "p256dh":p256dh_key,
        "auth":auth_key
    }
}


console.log(fcm_dict)

var push = require('web-push');


let vapidKeys = {publicKey: 'BIXkoWIy-Civu0JNgp_L54EtZcLik1UV0Cqfk1cgdP93e0FNkVx_Wq5mADIotnSTx9NlrLzZ5kNMY64Mz5TVqOc',
    privateKey: 'RQkSIUlBT2PBESlLrbfDsKNrUdHt5ynZsNUVJ3ES9DE'}

push.setVapidDetails(
    'mailto:example@yourdomain.org',
    vapidKeys.publicKey,
    vapidKeys.privateKey
  );

//pull from db the pushSubscription = //result of line 41 login.vue console.log(JSON.stringify(push));
// subscription = {"endpoint":"https://fcm.googleapis.com/fcm/send/ebuaaFCZh2k:APA91bG5_yGNejq1uk9FmbanJbDUx_PYeyNpG2dK2eUCBmMfVoKYpI1bSgs-QNzRLvCwA_JpUvP0BlmQXi_9cfjZt9juWZxosr7M_P_7d2Xk9ooskyIRv10_bXrqXS57WgS4of0uQM-G","expirationTime":null,"keys":{"p256dh":"BNPVzjnpO-zcRM43xuCbyqh_vo9TZGWLRwv7rH1FVV7soLfMPwUiUIieySxYCVagPbSyHkHETqDwtkiNIOElXvM","auth":"RSXwXOC-4F2DW2UyoW2RMg"}}
subscription = fcm_dict;

const payload = JSON.stringify( {title: "hossein's push-test msg" } );
push
    .sendNotification(subscription, payload)
    .catch(err => console.error(err));





//how to generate the keys
//public and private keys - generate only once
//let vapidKeys = push.generateVAPIDKeys();

//public key paste on web app side
//console.log(vapidKeys);

/*result:
hosseinb@HosseinBs-MacBook-Air src % node push.js
{
    publicKey: 'BIXkoWIy-Civu0JNgp_L54EtZcLik1UV0Cqfk1cgdP93e0FNkVx_Wq5mADIotnSTx9NlrLzZ5kNMY64Mz5TVqOc',
    privateKey: 'RQkSIUlBT2PBESlLrbfDsKNrUdHt5ynZsNUVJ3ES9DE'
}*/