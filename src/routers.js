import { createRouter, createWebHistory } from 'vue-router';
import TokenManagement from './components/TokenManagement.vue';
import Login from './components/Login.vue';
import AddTokenManually from './components/AddTokenManually.vue';


const routes = [
    {
        name: 'Login',
        component: Login,
        path: '/'
    },
    {
        name: 'TokenManagement',
        component: TokenManagement,
        path: '/token-management'
    },
    {
        name: 'AddTokenManually',
        component: AddTokenManually,
        path: '/token-management/add-token-manually'
        // path: '/add-token-manually'
    }
];

const router = createRouter(
    {
        history: createWebHistory(),
        routes
    }
);

export default router;

