// to be shared with all components that need it 
// instead of creating child / parents and keeping track 
// using view router instead 
import {ref} from "vue";

export const activePage = ref({
    currentPage:'',
    setCurrentPage(valueString){
        this.currentPage = valueString;
        console.log("active page is", this.currentPage)
    }
});